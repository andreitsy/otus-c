#ifndef __FIND_EOCDR_H
#define __FIND_EOCDR_H

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#pragma pack(push, 1)
struct eocdr { /* End of Central Directory Record. */
    uint16_t disk_number; /* Number of this disk. */
    uint16_t start_disk_number; /* Nbr. of disk with start of the CD. */
    uint16_t number_cd_record;  /* Nbr. of CD entries on this disk. */
    uint16_t total_cd_record; /* Nbr. of Central Directory entries. */
    uint32_t size_of_cd; /* Central Directory size in bytes. */
    uint32_t cd_offset; /* Central Directory file offset. */
    uint16_t comment_length; /* Archive comment length. */
    uint8_t *comment; /* Archive comment length. */
};

struct cfh { /* Central File Header (Central Directory Entry) */
    uint16_t made_by_ver;    /* version made by. */
    uint16_t extract_ver;    /* Version needed to extract. */
    uint16_t gp_flag;        /* General purpose bit flag. */
    uint16_t method;         /* Compression method. */
    uint16_t mod_time;       /* Modification time. */
    uint16_t mod_date;       /* Modification date. */
    uint32_t crc32;          /* CRC-32 checksum. */
    uint32_t comp_size;      /* Compressed size. */
    uint32_t uncomp_size;    /* Uncompressed size. */
    uint16_t name_len;       /* Filename length. */
    uint16_t extra_len;      /* Extra data length. */
    uint16_t comment_len;    /* Comment length. */
    uint16_t disk_nbr_start; /* Disk nbr. where file begins. */
    uint16_t int_attrs;      /* Internal file attributes. */
    uint32_t ext_attrs;      /* External file attributes. */
    uint32_t lfh_offset;     /* Local File Header offset. */
    const uint8_t *name;     /* Filename. */
    const uint8_t *extra;    /* Extra data. */
    const uint8_t *comment;  /* File comment. */
};
#pragma pack(pop)

bool read_eocdr(const uint8_t *fsrc, size_t fsize, struct eocdr *r);

bool read_cfh(struct cfh *cfh, const uint8_t *src, size_t src_len, uint32_t offset);

bool zip_read_and_print_files(struct eocdr *eocdr, const uint8_t *src, size_t src_len);

size_t find_offset_cfh(size_t offset, const uint8_t *src, size_t src_len);

#endif // __FIND_EOCDR_H
