#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include "zip_read.h"

uint8_t *read_file_in_memory(FILE *f_in, size_t *fsize);

void clean_up(FILE *f_in, uint8_t *data_content, size_t fsize);

int main(int argc, char **argv) {
    if (argc != 2) {
        printf("Usage: %s <file_path> \n", argv[0]);
        return EXIT_FAILURE;
    }
    size_t fsize = 0;
    uint8_t *data_content = NULL;
    const char *fpath = argv[1];

    FILE *f_in = fopen(fpath, "rb");
    if (f_in == NULL) {
        printf("File %s not exist!\n", fpath);
        return EXIT_FAILURE;
    }
    data_content = read_file_in_memory(f_in, &fsize);

    if (data_content != NULL) {
        // start reading from end of central dir
        struct eocdr eocdr;
        if (read_eocdr(data_content, fsize, &eocdr)) {
            printf("File is zipjpeg or zip with %d file(s) or archive(s)\n"
                   "It contains the followings files/directories:\n",
                   eocdr.total_cd_record);
            zip_read_and_print_files(&eocdr, data_content, fsize);
        } else {
            printf("File is NOT zipjpeg\n");
        }
    }
    clean_up(f_in, data_content, fsize);
}


void clean_up(FILE *f_in, uint8_t *data_content, size_t fsize) {
    if (data_content != NULL) {

        munmap(data_content, fsize);
        data_content = NULL;
    }
    fclose(f_in);
}

uint8_t *read_file_in_memory(FILE *f_in, size_t *fsize) {
    struct stat stbuf;
    int fd = fileno(f_in);
    if ((fstat(fd, &stbuf) != 0) || (!S_ISREG(stbuf.st_mode))) {
        printf("Cannot open file!\n");
    }
    *fsize = stbuf.st_size;
    uint8_t *data = mmap(NULL, *fsize, PROT_READ, MAP_PRIVATE, fd, 0);

    if (data == MAP_FAILED) {
        printf("File is too big to malloc memory!");
        return NULL;
    }
    return data;
}
