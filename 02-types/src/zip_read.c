#include "zip_read.h"

/* Size of the End of Central Directory Record not including comment. */
#define EOCDR_BASE_SZ 22
#define CFH_BASE_SZ 46
#define EOCDR_SIGNATURE 0x06054b50  /* "PK\5\6" little-endian. */
#define CFH_SIGNATURE 0x02014b50 /* "PK\1\2" little-endian. */
#define ZIP_STORED 0
#define ZIP_DEFLATED 8

/* Read 16/32 bits little-endian and bump p forward afterwards. */
#define read_bits_b(x, p, b) (((p)+=b), (memcpy((x), (p - (b)), (b))))

bool read_eocdr(const uint8_t *fsrc, size_t fsize, struct eocdr *r) {
    size_t comment_length;
    const uint8_t *p;
    uint32_t signature;

    for (comment_length = 0; comment_length <= UINT16_MAX; comment_length++) {
        if (fsize < EOCDR_BASE_SZ + comment_length) {
            break;
        }

        p = &fsrc[fsize - EOCDR_BASE_SZ - comment_length];
        read_bits_b(&signature, p, 4);

        if (signature == EOCDR_SIGNATURE) {
            memcpy(r, p, sizeof(struct eocdr));
            if (r->comment_length == comment_length) {
                return true;
            }
        }
    }
    return false;
}

size_t find_offset_cfh(size_t offset, const uint8_t *src,
                       size_t src_len) {
    uint32_t signature = 0;
    const uint8_t *p = NULL;
    while ((signature != CFH_SIGNATURE) && offset < src_len) {
        p = &src[offset];
        read_bits_b(&signature, p, 4);
        offset++;
    }
    return offset - 1;
}


bool read_cfh(struct cfh *cfh, const uint8_t *src,
              size_t src_len, uint32_t offset) {
    const uint8_t *p;
    uint32_t signature;

    if (offset > src_len || src_len - offset < CFH_BASE_SZ) {
        return false;
    }

    p = &src[offset];
    read_bits_b(&signature, p, 4);
    if (signature != CFH_SIGNATURE) {
        return false;
    }
    memcpy(cfh, p, sizeof(struct cfh));
    p += 42;
    cfh->name = p;
    cfh->extra = cfh->name + cfh->name_len;
    cfh->comment = cfh->extra + cfh->extra_len;

    if ((src_len - offset - CFH_BASE_SZ) < (size_t) (cfh->name_len + cfh->extra_len + cfh->comment_len)) {
        return false;
    }

    return true;
}

bool zip_read_and_print_files(struct eocdr *eocdr, const uint8_t *src, size_t src_len) {
    struct cfh cfh;
    size_t i, offset;

    if (eocdr->disk_number != 0 || eocdr->start_disk_number != 0 ||
        eocdr->total_cd_record != eocdr->number_cd_record) {
        return false; /* Cannot handle multi-volume archives. */
    }

    offset = find_offset_cfh(eocdr->cd_offset, src, src_len);
    /* Read the member info, print and do a few checks. */
    for (i = 0; i < eocdr->total_cd_record; i++) {
        if (!read_cfh(&cfh, src, src_len, offset)) {
            return false;
        }
        if (cfh.gp_flag & 1) {
            return false; /* The member is encrypted. */
        }
        if (cfh.method != ZIP_STORED && cfh.method != ZIP_DEFLATED) {
            return false; /* Unsupported compression method. */
        }
        if (cfh.method == ZIP_STORED &&
            cfh.uncomp_size != cfh.comp_size) {
            return false;
        }
        if (cfh.disk_nbr_start != 0) {
            return false; /* Cannot handle multi-volume archives. */
        }
        if (memchr(cfh.name, '\0', cfh.name_len) != NULL) {
            return false; /* Bad filename. */
        }
        printf("  ");
        for (uint16_t i = 0; i < cfh.name_len; ++i) {
            printf("%c", (char) cfh.name[i]);
        }
        printf("\n");

        offset += CFH_BASE_SZ + cfh.name_len + cfh.extra_len +
                  cfh.comment_len;
    }
    return true;
}
