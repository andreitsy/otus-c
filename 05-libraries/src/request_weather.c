#include "request_weather.h"

struct MemoryStruct {
    char *memory;
    size_t size;
};

size_t WriteMemoryCallback(void *contents, size_t size,
                           size_t nmemb, void *userp);

char *do_request(const char *URL);

char *get_url_api_location(const char *location);

char *get_url_api_weather(const char *woeid);

char *get_woeid(const char *location);

size_t WriteMemoryCallback(void *contents, size_t size,
                           size_t nmemb, void *userp) {
    size_t realsize = size * nmemb;
    struct MemoryStruct *mem = (struct MemoryStruct *) userp;

    char *ptr = realloc(mem->memory, mem->size + realsize + 1);
    if (!ptr) {
        /* out of memory! */
        printf("not enough memory (realloc returned NULL)\n");
        return 0;
    }

    mem->memory = ptr;
    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}

char *do_request(const char *url) {
    CURL *curl_handle;
    CURLcode res;
    struct MemoryStruct chunk;

    curl_global_init(CURL_GLOBAL_ALL);
    curl_handle = curl_easy_init();
    if (!curl_handle) {
        perror("Cannot init cur lib!\n");
        return NULL;
    }

    chunk.memory = NULL;  /* will be grown by the realloc above */
    chunk.size = 0;    /* no data at this point */

    /* specify URL to get */
    curl_easy_setopt(curl_handle, CURLOPT_URL, url);
    /* send all data to this function  */
    curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
    /* we pass our 'chunk' struct to the callback function */
    curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *) &chunk);
    /* some servers do not like requests that are made without a user-agent
       field, so we provide one */
    curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");

    res = curl_easy_perform(curl_handle);
    /* check for errors */
    if (res != CURLE_OK) {
        fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));
    }
    curl_easy_cleanup(curl_handle);
    /* we are done with libcurl, so clean it up */
    curl_global_cleanup();
    return chunk.memory;
}

char *get_url_api_location(const char *location) {
    const char base_url[] = "https://www.metaweather.com/api/location/search/?query=";
    size_t message_len =  /* + 1 for terminating NULL */
            strlen(base_url) + strlen(location) + 1;
    char *url = (char *) calloc(message_len, sizeof(char));
    strncat(url, base_url, strlen(base_url));
    strncat(url, location, strlen(location));
    return url;
}

char *get_url_api_weather(const char *woeid) {
    const char base_url[] = "https://www.metaweather.com/api/location/";
    size_t message_len =  /* + 2 for terminating NULL and for '\' */
            strlen(base_url) + strlen(woeid) + 2;
    char *url = (char *) calloc(message_len, sizeof(char));
    strncat(url, base_url, strlen(base_url));
    strncat(url, woeid, strlen(woeid));
    url[message_len - 2] = '/';
    return url;
}

char *get_woeid(const char *location) {
    char *res;
    char *url_location = get_url_api_location(location);
    char *json_raw = do_request(url_location);
    json_object *json_location = json_tokener_parse(json_raw);
    size_t array_len = json_object_array_length(json_location);
    if (array_len == 0) {
        return NULL;
    }
    json_object *element = json_object_array_get_idx(json_location, 0);

    json_object_object_get_ex(element, "woeid", &element);

    res = (char *) calloc(strlen(json_object_to_json_string(element)) + 1, sizeof(char));
    strncat(res, json_object_to_json_string(element),
            strlen(json_object_to_json_string(element)) + 1);

    json_object_put(json_location);
    free(url_location);
    free(json_raw);
    return res;
}

char *get_json_weather(const char *location) {
    char *woeid = get_woeid(location);
    if (woeid == NULL) {
        return NULL;
    }
    char *url_weather = get_url_api_weather(woeid);
    char *json_raw = do_request(url_weather);
    free(woeid);
    free(url_weather);
    return json_raw;
}

void parse_json_and_print_info(char *weather_json) {
    json_object *json_root = json_tokener_parse(weather_json);
    json_object *json_consolidated_weather;
    json_object *json_day_weather;
    json_object_object_get_ex(json_root, "consolidated_weather",
                              &json_consolidated_weather);
    size_t array_len = json_object_array_length(json_consolidated_weather);
    printf("The forecast for %s:\n\n", json_object_get_string(
            json_object_object_get(json_root, "title")));
    for (size_t i = 0; i < array_len; i++) {
        // get the i-th object
        json_day_weather = json_object_array_get_idx(json_consolidated_weather, i);
        printf("%s:\n", json_object_get_string(
                json_object_object_get(json_day_weather, "applicable_date")));
        printf("  weather: %s\n", json_object_get_string(
                json_object_object_get(json_day_weather, "weather_state_name")));
        printf("  air pressure: %.2f mbar\n", json_object_get_double(
                json_object_object_get(json_day_weather, "air_pressure")));
        printf("  humidity: %.1f %%\n", json_object_get_double(
                json_object_object_get(json_day_weather, "humidity")));
        printf("  visibility: %.3f miles\n", json_object_get_double(
                json_object_object_get(json_day_weather, "visibility")));
        printf("  wind direction compass: %s\n", json_object_get_string(
                json_object_object_get(json_day_weather, "wind_direction_compass")));
        printf("  wind direction: %.2f degrees\n", json_object_get_double(
                json_object_object_get(json_day_weather, "wind_direction")));
        printf("  wind speed: %.2f mph\n", json_object_get_double(
                json_object_object_get(json_day_weather, "wind_speed")));
        printf("  max temperature: %.2f centigrade\n", json_object_get_double(
                json_object_object_get(json_day_weather, "max_temp")));
        printf("  min temperature: %.2f centigrade\n", json_object_get_double(
                json_object_object_get(json_day_weather, "min_temp")));
        printf("  the temperature: %.2f centigrade\n", json_object_get_double(
                json_object_object_get(json_day_weather, "the_temp")));
    }
    json_object_put(json_root);
}
