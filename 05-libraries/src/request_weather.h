#ifndef WEATHER_REQUEST_WEATHER_H
#define WEATHER_REQUEST_WEATHER_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <json-c/json.h>


char *get_json_weather(const char *location);

void parse_json_and_print_info(char *weather_json);

#endif //WEATHER_REQUEST_WEATHER_H
