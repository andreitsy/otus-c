#include <stdio.h>
#include <stdlib.h>
#include "request_weather.h"

int main(int argc, char **argv) {
    if (argc != 2) {
        printf("Usage: %s <location> \n", argv[0]);
        return EXIT_FAILURE;
    }
    const char *location = argv[1];

    char *weather_json = get_json_weather(location);
    if (weather_json == NULL) {
        printf("Not found given location '%s'\n", location);
        return EXIT_FAILURE;
    }
    parse_json_and_print_info(weather_json);

    free(weather_json);
    return EXIT_SUCCESS;
}
