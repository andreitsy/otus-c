# Структуры данных, домашнее задание №5

Требуется библиотека libcurl.
Для компиляции можно воспользоваться cmake:

```
$ cd 05-libraries
$ mkdir build
$ cd build
$ cmake ..
$ make
```

Кроме того нужно установить библиотеку [json-c](https://github.com/json-c/json-c)
```
$ cd 05-libraries
$ git clone https://github.com/json-c/json-c.git
$ mkdir json-c-build
$ cd json-c-build
$ cmake ../json-c
$ make
$ sudo make install
```

Полученная программа `weather` может быть запущена таким образом:
```
$ ./build/weather moscow
The forecast for Moscow:

2022-01-29:
  weather: Snow
  air pressure: 1004.50 mbar
  humidity: 92.0 %
  visibility: 3.625 miles
  wind direction compass: SE
  wind direction: 132.01 degrees
  wind speed: 2.52 mph
  max temperature: -3.58 centigrade
  min temperature: -11.73 centigrade
  the temperature: -3.00 centigrade
...
```

В результате будет выведен прогноз погоды в данном регионе.