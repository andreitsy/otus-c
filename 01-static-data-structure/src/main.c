#include <stdio.h>
#include <stdlib.h>
#include "converter.h"
#include "encodings.h"

int main(int argc, char** argv) 
{
    if (argc != 4)
    {
        printf("Usage: %s <file_path_in> <file_path_out> <encoding>\n", argv[0]);
        return EXIT_FAILURE;
    }
    const char* fpath_in = argv[1];
    const char* fpath_out = argv[2];
    enum Encodings encoding = str_to_encoding(argv[3]);

    if (encoding >= 0)
    {
        convert(fpath_in, fpath_out, encoding);
        return EXIT_SUCCESS;
    } 
    else
    {
        return EXIT_FAILURE;
    }

}
