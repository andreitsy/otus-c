#ifndef __CONVERTER_H
#define __CONVERTER_H
#include "encodings.h"

int convert(const char* fpath_in, const char* fpath_out, enum Encodings encoding);

#endif // __CONVERTER_H

