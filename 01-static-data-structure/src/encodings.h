#ifndef __ENCODINGS_H
#define __ENCODINGS_H

enum Encodings {
	cp1251,
	koi8,
	iso8859_5
};
enum Encodings str_to_encoding(const char * str_encoding);

const unsigned int cp1251_table[66][2];
const unsigned int iso8859_5_table[66][2];
const unsigned int koi8_table[66][2];

#endif // __ENCODINGS_H

