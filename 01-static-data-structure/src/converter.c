#include "converter.h"
#include "encodings.h"
#include <stdio.h>
#include <stdlib.h>


void convert_file_by_table(FILE* f_in, FILE* f_out, const unsigned int table[66][2])
{
	unsigned int in_c = 0;
	unsigned int out_c = 0;
	do 
	{
		in_c = fgetc(f_in);
		if (feof(f_in)) { break;}
		
		out_c = in_c;
		for (int i = 0; i < 66; ++i)
		{
			if (in_c == table[i][0]) 
			{
				out_c = table[i][1];
				break;
			}
		}
		if (out_c & 0xff00) 
		{
			fputc((out_c & 0xff00) >> 8, f_out);
		}
		fputc(out_c & 0xff, f_out);
	
	} while(1);	
}

int convert(const char* fpath_in, const char* fpath_out, enum Encodings encoding)
{
    FILE *f_in;
    FILE *f_out;
    
    f_in = fopen(fpath_in, "rb");
    if (f_in == NULL)
    {
	    printf("File %s not exist!\n", fpath_in);
	    return 1;
    }	    
    
    f_out = fopen(fpath_out,"wb+");
    if (f_out == NULL)
    {
	    printf("File %s not exist!\n", fpath_out);
	    return 2;
    }

    switch(encoding)
    {
	case cp1251:
        	convert_file_by_table(f_in, f_out, cp1251_table);
        	break;
	case iso8859_5:
		convert_file_by_table(f_in, f_out, iso8859_5_table);
		break;
	case koi8:
		convert_file_by_table(f_in, f_out, koi8_table);
		break;
	default:
		printf("Invalid encoding in function convert!\n");
		return 3;
    }

    fclose(f_in);
    fclose(f_out); 
    return 0;
}

