# -*- coding: utf-8 -*-
alphabet_rus = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
alphabet_rus += alphabet_rus.lower()

from_encoding = 'iso-8859-5'

with open("all.txt", "w+") as f:
   f.write("const int " + from_encoding + "_table[][2] = {\n")
   for x in alphabet_rus:
      f.write("{" + hex(int.from_bytes(x.encode(from_encoding), "big")) + "," +
              hex(int.from_bytes(x.encode('utf-8'), "big")) + "}, // " + x + "\n")
   f.write("}")

