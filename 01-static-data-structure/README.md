# Статические структуры данных, домашнее задание №1

Для компиляции можно воспользоваться cmake:

```
cd 01-static-data-structure
mkdir build
cd build
cmake ..
make
```

Полученная программка otus-convertor может быть запущена таким образом:
```
./otus-convertor ../files/cp1251.txt utf8.txt cp-1251
```

В результате будет сгенерирован файл 'utf8.txt' в кодировке utf-8

