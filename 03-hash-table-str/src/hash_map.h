#ifndef __HASH_MAP_H
#define __HASH_MAP_H

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>


#define MAX_WORD_LEN 255

typedef struct {
    int val;
    char key[MAX_WORD_LEN];
} hash;

unsigned long long compute_hash(const char *word);

size_t find_open_address(const hash *p_hash, const char *key);

bool contains(const char *key, const hash *p_hash);

int get(const char *key, const hash *p);

void add(const char *key, int val, hash *p_hash);

void set(const char *key, int val, hash *p_hash);

void print_counts_words(hash *p_hash);

hash *init_hash_map();

void free_hash_map(hash *p);

#endif // __HASH_MAP_H
