#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "hash_map.h"

#define IN_WORD 1 /* inside word */
#define OUT_WORD 2 /* outside word */

void read_words_to_hash_map(FILE *file_words, hash *p_hash);

void add_to_hash(char *word, hash *p_hash);

int main(int argc, char **argv) {
    if (argc != 2) {
        printf("Usage: %s <file_path> \n", argv[0]);
        return EXIT_FAILURE;
    }
    const char *fpath = argv[1];

    FILE *f_in = fopen(fpath, "rb");
    if (f_in == NULL) {
        printf("File %s not exist!\n", fpath);
        return EXIT_FAILURE;
    }

    hash *p_hash = init_hash_map();
    read_words_to_hash_map(f_in, p_hash);

    printf("The following words and frequencies are in %s:\n", argv[1]);
    print_counts_words(p_hash);

    free_hash_map(p_hash);
    fclose(f_in);
}

void add_to_hash(char *word, hash *p_hash) {
    if (contains(word, p_hash)) {
        int val = get(word, p_hash);
        set(word, val + 1, p_hash);
    } else {
        add(word, 1, p_hash);
    }
}

void read_words_to_hash_map(FILE *file_words, hash *p_hash) {
    uint8_t word_len = 0, state = OUT_WORD;
    char c;
    char word[MAX_WORD_LEN] = {'\0'};

    while ((c = fgetc(file_words)) != EOF) {
        if (isspace(c)) {
            state = OUT_WORD;
            word[word_len] = '\0';
            if (word_len > 0) {
                add_to_hash(word, p_hash);
                word_len = 0;
            }
        } else {
            if (state == OUT_WORD) {
                state = IN_WORD;
                word_len = 0;
            }
            word[word_len] = c;
            word_len++;
            if (word_len >= MAX_WORD_LEN) {
                word[MAX_WORD_LEN - 1] = '\0';
                fprintf(stderr, "Error: too long word which starting with '%s'\n", word);
                exit(EXIT_FAILURE);
            }
        }
        if (feof(file_words)) {
            break;
        }
    }
    if (word_len > 0) {
        add_to_hash(word, p_hash);
        word_len = 0;
    }
}
