#include "hash_map.h"

#define MAX_SIZE_HASH 31441
#define K_CONST 17 // gcd(K_CONST, MAX_SIZE_HASH) = 1

hash *init_hash_map() {
    hash *rez = (hash *) calloc(MAX_SIZE_HASH, sizeof(hash));
    return rez;
}

void free_hash_map(hash *p) {
    free(p);
}

void print_counts_words(hash *p_hash) {
    for (int i = 0; i < MAX_SIZE_HASH; ++i) {
        if (strlen(p_hash[i].key) > 0) {
            printf(" %s: %d\n", p_hash[i].key, p_hash[i].val);
        }
    }
}

unsigned long long compute_hash(const char *word) {
    const int power = 31;
    unsigned long long hash = 0, p_pow = 1;

    if (word) {
        while ((*word != '\0') && isspace(*word)) {
            hash += (*word - 'a' + 1) * p_pow;
            p_pow *= power;
            ++word;
        }
        return hash;
    } else {
        perror("Error: incorrect key for word");
        return 0;
    }
}

size_t find_open_address(const hash *p_hash, const char *key) {
    unsigned long long hash_val = compute_hash(key);
    for (unsigned int i = 0; i < MAX_SIZE_HASH; ++i) {
        size_t addr = (hash_val + K_CONST * i) % MAX_SIZE_HASH;
        if (strcmp(key, p_hash[addr].key) == 0) {
            return addr;
        }
    }
    return MAX_SIZE_HASH;
}

bool contains(const char *key, const hash *p_hash) {
    if (find_open_address(p_hash, key) < MAX_SIZE_HASH) {
        return true;
    } else {
        return false;
    }
}

int get(const char *key, const hash *p_hash) {
    size_t addr = find_open_address(p_hash, key);
    return p_hash[addr].val;
}

void set(const char *key, int val, hash *p_hash) {
    size_t addr = find_open_address(p_hash, key);
    (p_hash + addr)->val = val;
}

void add(const char *key, int val, hash *p_hash) {
    unsigned long long hash_val = compute_hash(key);

    for (unsigned int i = 0; i < MAX_SIZE_HASH; ++i) {
        size_t addr = (hash_val + K_CONST * i) % MAX_SIZE_HASH;

        if (strlen(p_hash[addr].key) == 0) {
            strcpy(p_hash[addr].key, key);
            p_hash[addr].val = val;
            break;
        }
    }
}
