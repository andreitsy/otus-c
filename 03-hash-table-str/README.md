# Структуры данных, домашнее задание №3

Для компиляции можно воспользоваться cmake:

```
cd 03-hash-table-str
mkdir build
cd build
cmake ..
make
```

Полученная программка count_words может быть запущена таким образом:
```
$ ./build/count_words ./resources/file.txt
The following words and frequencies are in ./resources/file.txt:
 over: 4
 park: 2
 flood: 1
 pale: 1
 hill: 1
 Thorough: 1
 thorough: 3
 brier: 1
 bush: 1
 dale: 1
 fire: 3
```

В результате будет выведен на экран список слов и их частоты.

