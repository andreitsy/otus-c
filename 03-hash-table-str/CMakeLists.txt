cmake_minimum_required(VERSION 3.5)
project(otus-convertor C)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_FLAGS  "-Wall -Wextra -Wpedantic -std=c11")
set(CMAKE_C_FLAGS_DEBUG "-g")

set(SOURCES
    "src/main.c"
    "src/hash_map.c"
)

add_executable(count_words ${SOURCES})
